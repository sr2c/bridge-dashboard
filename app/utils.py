import base64
import logging
from datetime import datetime
import io
import json
from typing import List, Any, Dict

import flask
import pandas as pd
import requests
from flask import url_for, redirect
from flask_dance.contrib.azure import azure
from matplotlib.figure import Figure
from oauthlib.oauth2 import TokenExpiredError
from sqlalchemy import desc
from sqlalchemy.sql import func
from werkzeug.utils import redirect

from app.extensions import db, login
from app.models import Bridge, UserCounts, ByteCounts, LastUpdates


def format_update_info(query_data: List[LastUpdates]) -> str:
    s = []
    for result in query_data:
        service = result.service.split('_')[-2].capitalize()
        last_updated = result.last_updated.strftime("%m/%d/%Y, %H:%M:%S")
        s.append(f"{service} last updated at {last_updated}")
    return "<br>".join(s)


def encode_image_b64(fig: Figure, form: str = 'png') -> str:
    """
    Renders a matplotlib.figure.Figure as an image and returns
    the data as a base64 encoded str.
    """
    buffer = io.BytesIO()
    fig.savefig(buffer, dpi=150, format=form)
    buffer.seek(0)
    data = base64.b64encode(buffer.getvalue())
    return data.decode('utf-8')


def load_json(filename: str) -> Dict[str, Any]:
    """
    Loads JSON from a file and returns it parsed.
    """
    with open(filename) as f:
        j = json.load(f)
    return j


def update_bridgestrap_data(app: flask.Flask) -> None:
    with app.app_context():
        bridges = Bridge.query.order_by(desc(Bridge.last_restarted)).all()
        for bridge in bridges:
            logging.debug("Bridgestrapping %s", bridge.fingerprint)
            try:
                response = requests.get('http://192.168.2.28:5000/bridge-state',
                                        json={"bridge_lines": [f"{bridge.bridgeline}"]}, timeout=70)
                bridge.bridgestrap = str(response.json()['bridge_results'][bridge.bridgeline]["functional"])
                bridge.last_bridgestrap_update = datetime.strptime(
                    response.json()['bridge_results'][bridge.bridgeline]["last_tested"], "%Y-%m-%dT%H:%M:%S.%fZ")
            except requests.exceptions.Timeout:
                logging.exception("Bridgestrap failed to respond within 70 seconds.")
            db.session.commit()


def update_onionoo_data(app: flask.Flask) -> datetime:
    with app.app_context():
        logging.debug("Updating data from Onionoo")
        bridges = Bridge.query.order_by(desc(Bridge.last_restarted)).all()
        response = requests.get(f"https://onionoo.torproject.org/details?search=type:bridge")
        onionoo = response.json()['bridges']
        for bridge in bridges:
            stats = dict()
            for b in range(len(onionoo)):
                stats[onionoo[b]["hashed_fingerprint"]] = onionoo[b]
            if len(stats) > 0:
                if bridge.hashed_fingerprint in stats:
                    if 'overload_general_timestamp' in stats[bridge.hashed_fingerprint]:
                        bridge.overloaded = True
                    else:
                        bridge.overloaded = False
                    bridge.last_onionoo_update = datetime.now()
                    bridge.adv_bandwidth = round(stats[bridge.hashed_fingerprint]['advertised_bandwidth'] / 1048576, 2)
                    bridge.last_restarted = datetime.strptime(stats[bridge.hashed_fingerprint]['last_restarted'],
                                                          '%Y-%m-%d %H:%M:%S')
                    bridge.last_seen = datetime.strptime(stats[bridge.hashed_fingerprint]['last_seen'],
                                                     '%Y-%m-%d %H:%M:%S')
                    bridge.running = stats[bridge.hashed_fingerprint]['running']
                    bridge.version = stats[bridge.hashed_fingerprint]['version']
                    bridge.first_seen = datetime.strptime(stats[bridge.hashed_fingerprint]['first_seen'],
                                                      '%Y-%m-%d %H:%M:%S')
                    bridge.flags = " ".join(stats[bridge.hashed_fingerprint]['flags'])

                else:
                    bridge.running = False
                    bridge.version = 'N/A'
                    bridge.active = False
                    bridge.adv_bandwidth = 0
                db.session.add(bridge)
                db.session.commit()
        return datetime.now()


def avg_client_graph(bridge: Bridge) -> str:
    total_users = db.session.query(func.sum(UserCounts.total_users).label('msum'),
                                   func.date(UserCounts.date_end).label('mdate')). \
        filter(UserCounts.fingerprint == bridge.fingerprint,
               func.DATE(UserCounts.date_end) < datetime.now().date()).group_by(UserCounts.date_end).all()
    df = pd.DataFrame.from_records(total_users)
    df.columns = ["users", "date"]
    fig = Figure(figsize=(6, 4))
    ax = fig.subplots()
    ax.plot(df.date, df.users, label="Users per day")
    fig.autofmt_xdate(rotation=45)
    ax.legend()
    data = encode_image_b64(fig)
    return data


def bytes_client_graph(bridge: Bridge) -> str:
    total_read_bytes = db.session.query(ByteCounts.bytes_read, ByteCounts.bytes_written,
                                        func.date(ByteCounts.date_end).label('mdate')). \
        filter(ByteCounts.fingerprint == bridge.fingerprint,
               func.DATE(ByteCounts.date_end) < datetime.now().date()).group_by(ByteCounts.date_end).all()
    df = pd.DataFrame.from_records(total_read_bytes)
    df.columns = ["rbytes", "wbytes", "date"]
    fig = Figure(figsize=(6, 4))
    ax = fig.subplots()
    ax.plot(df.date, df.rbytes, label="Read bytes per day")
    ax.plot(df.date, df.wbytes, label="Written bytes per day")
    fig.autofmt_xdate(rotation=45)
    ax.legend()
    data = encode_image_b64(fig)
    return data


def auth(f):
    def auth_wrapper(*args, **kwargs):
        if not azure.authorized:
            return redirect(url_for("azure.login"))
        try:
            a = azure.get("/v1.0/me")
            assert a.ok
        except TokenExpiredError:
            del login.token
            return redirect(url_for("azure.login"))
        assert a.ok
        return f(*args, **kwargs, user=a.json())

    auth_wrapper.__name__ = f.__name__
    return auth_wrapper


def noauth(f):
    def auth_wrapper(*args, **kwargs):
        if not azure.authorized:
            return f(*args, **kwargs, user=None)
        try:
            a = azure.get("/v1.0/me")
            assert a.ok
        except TokenExpiredError:
            del login.token
            return redirect(url_for("azure.login"))
        return f(*args, **kwargs, user=a.json())

    auth_wrapper.__name__ = f.__name__
    return auth_wrapper

def update_from_json():
    # output_path =  os.path.expanduser("~")
    bridges = load_json(dashboard.config["DEFAULT_BRIDGE_FILE"])

    for o in bridges["values"]["outputs"]:
        for b in bridges["values"]["outputs"][o]["value"]:
            bridgelineparts = bridges["values"]["outputs"][o]["value"][b]["bridgeline"].split(" ")
            bridgeline = [part for part in bridgelineparts if (part != "Bridge" and not part.startswith("Sr"))]
            bridgeline = " ".join(bridgeline)

            fingerprint = bridges["values"]["outputs"][o]["value"][b]["fingerprint_rsa"].split(" ")[-1]
            hashed_fingerprint = bridges["values"]["outputs"][o]["value"][b]["hashed_fingerprint"].split(" ")[-1]

            br = Bridge(nickname=bridges["values"]["outputs"][o]["value"][b]["nickname"],
                        fingerprint=fingerprint,
                        hashed_fingerprint=hashed_fingerprint,
                        obfs_port=bridges["values"]["outputs"][o]["value"][b]["obfs_port"],
                        ip_address=bridges["values"]["outputs"][o]["value"][b]["ip_address"],
                        bridgeline=bridgeline
                        )
            exists = Bridge.query.filter_by(fingerprint=fingerprint).first()
            if not exists:
                db.session.add(br)
        db.session.commit()