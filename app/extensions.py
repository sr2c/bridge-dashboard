import os

from flask_bootstrap import Bootstrap
from flask_dance.contrib.azure import make_azure_blueprint
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()
bootstrap = Bootstrap()

login = make_azure_blueprint(
    client_id=os.environ.get('AZURE_OAUTH_CLIENT_ID', '043381c9-8352-4950-ab8a-bb262c79aab1'),
    client_secret=os.environ.get('AZURE_OAUTH_CLIENT_SECRET', 'None'),
    tenant=os.environ.get('AZURE_OAUTH_TENANT_ID', '9c4a673c-4923-42c9-9a7a-4513851e0808'))
