from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, EqualTo


class EditGroupForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    provider = SelectField('Provider', validators=[DataRequired()])
    distribution_method = SelectField('Distribution Method', validators=[DataRequired()])
    description = StringField('Description')
    submit = SubmitField('Add Group')


class DeleteGroupForm(FlaskForm):
    name = SelectField('Name', validators=[DataRequired()])
    name_confirm = StringField('To confirm, type in the name of the group', validators=[DataRequired(), EqualTo('name', message='Name must be the same to delete a group!')])
    submit = SubmitField('Delete Group')