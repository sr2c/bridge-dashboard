import datetime

import apscheduler.events
from apscheduler.events import EVENT_JOB_ERROR, EVENT_JOB_EXECUTED
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix

from app.collector_utils import update_collector_data
from app.config import Config
from app.dashboard import dashboard
from app.extensions import db, migrate, bootstrap, login
from app.forms import EditGroupForm, DeleteGroupForm
from app.models import Bridge, UserCounts, LastUpdates, Group, Provider, DistributionMethod
from app.utils import load_json, update_onionoo_data, update_bridgestrap_data, avg_client_graph, \
    bytes_client_graph, format_update_info

LAST_UPDATED = dict()

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1)
app.config.from_object(Config)

db.init_app(app)
migrate.init_app(app, db, compare_type=True, render_as_batch=True)
bootstrap.init_app(app)

app.register_blueprint(dashboard)

app.register_blueprint(login, url_prefix="/login")

scheduler = BackgroundScheduler()


def jobs_listener(event: apscheduler.events.SchedulerEvent) -> None:
    with app.app_context():
        if not event.exception:
            job = scheduler.get_job(event.job_id)
            lu = LastUpdates(service=job.name, last_updated=datetime.datetime.now())
            exists = LastUpdates.query.filter_by(service=job.name).first()
            if exists:
                exists.last_updated = datetime.datetime.now()
            else:
                db.session.add(lu)
            db.session.commit()


scheduler.add_listener(jobs_listener, EVENT_JOB_EXECUTED | EVENT_JOB_ERROR)
job_collector = scheduler.add_job(update_collector_data, 'interval', minutes=60, misfire_grace_time=10 * 60, args=[app])
job_bridgestrap = scheduler.add_job(update_bridgestrap_data, 'interval', minutes=30, misfire_grace_time=10 * 60, args=[app])
job_onionoo = scheduler.add_job(update_onionoo_data, 'interval', minutes=15, misfire_grace_time=10 * 60, args=[app])
scheduler.start()


@app.template_filter()
def format_uptime(bridge: Bridge) -> str:
    if bridge.last_onionoo_update is None:
        return "N/A"
    if bridge.running:
        return str(datetime.datetime.now() - bridge.last_restarted)
    else:
        return f"Last seen {datetime.datetime.now() - bridge.last_seen} ago"


if __name__ == '__main__':
    app.run()
