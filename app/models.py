from app.extensions import db
import enum

class Provider(enum.Enum):
    OVH = "OVH"
    HETZNER = "Hetzner"
    GANDI = "Gandi Cloud VPS"
    NONE = "External"

class DistributionMethod(enum.Enum):
    MOAT = "moat"
    NONE = "None"


class Group(db.Model):
    name = db.Column(db.String(64), primary_key=True)
    description = db.Column(db.String(128))
    provider = db.Column(db.Enum(Provider))
    distribution_method= db.Column(db.Enum(DistributionMethod))
    bridges = db.relationship('Bridge', backref='bridge_group', lazy=True)

    def __repr__(self):
        return '<Group {}>'.format(self.name)

class Bridge(db.Model):
    id = db.Column(db.Integer, nullable=False, primary_key=True, unique=True, autoincrement=True)
    fingerprint = db.Column(db.String(128), unique=True)
    nickname = db.Column(db.String(64))
    name = db.Column(db.String(64))
    ip_address = db.Column(db.String(64))
    obfs_port = db.Column(db.Integer())
    active = db.Column(db.Boolean, default=True)
    running = db.Column(db.Boolean, default=False)
    overloaded = db.Column(db.Boolean, default=False)
    adv_bandwidth = db.Column(db.Float, default=0)
    version = db.Column(db.String)
    hashed_fingerprint = db.Column(db.String(128))
    last_seen = db.Column(db.DateTime)
    last_restarted = db.Column(db.DateTime)
    bridgeline = db.Column(db.String(512))
    last_onionoo_update = db.Column(db.DateTime())
    last_bridgestrap_update = db.Column(db.DateTime())
    bridgestrap = db.Column(db.Boolean, default=False)
    group = db.Column(db.String(64), db.ForeignKey('group.name'))
    first_seen = db.Column(db.DateTime)
    flags = db.Column(db.String(128))

    def set_group(self, group_name):
        self.group = group_name

    def __repr__(self):
        return '<Bridge {}>'.format(self.nickname)


class UserCounts(db.Model):
    id = db.Column(db.String(256), primary_key=True, unique=True, nullable=False)
    date_end = db.Column(db.DateTime, nullable=False)
    country = db.Column(db.String(64), nullable=False)
    total_users = db.Column(db.Integer(), nullable=False)
    fingerprint = db.Column(db.String(128), nullable=False)
    #id is made of descriptor_date_end, fingerprint and country

    def __repr__(self):
        return '<User Count {} {} {}>'.format(self.date_end, self.country, self.total_users)


class ByteCounts(db.Model):
    id = db.Column(db.String(256), primary_key=True, unique=True, nullable=False)
    date_end = db.Column(db.DateTime, nullable=False)
    bytes_written = db.Column(db.Float(), nullable=False)
    bytes_read = db.Column(db.Float(), nullable=False)
    fingerprint = db.Column(db.String(128), nullable=False)
    #id is made of descriptor_date_end and fingerprint

    def __repr__(self):
        return '<Bytes Count {} {} {} {}>'.format(self.date_end, self.fingerprint, self.bytes_written, self.bytes_read)


class LastUpdates(db.Model):
    service = db.Column(db.String(64), primary_key=True, unique=True, nullable=False)
    last_updated = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return '<{} last updated at {}>'.format(self.service, self.last_updated)
