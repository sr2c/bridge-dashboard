import datetime
from collections import defaultdict

import stem.descriptor.collector

from app.extensions import db
from app.models import Bridge, UserCounts, ByteCounts


def update_collector_data(app):
    with app.app_context():
        bridge_list = Bridge.query.all()
        yesterday = datetime.datetime.utcnow() - datetime.timedelta(days=10)

        stats = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))
        stats_bytes = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))

        for desc in stem.descriptor.collector.get_extrainfo_descriptors(bridge=True, start=yesterday):
            if desc.nickname.startswith("Sr2"):
                if desc.bridge_ips is not None and desc.bridge_stats_interval == 86400:
                    if desc.read_history_values:
                        stats_bytes[str(desc.bridge_stats_end)][desc.fingerprint]["hist_read"] = \
                        desc.read_history_values[-1] / desc.read_history_interval
                    if desc.write_history_values:
                        stats_bytes[str(desc.bridge_stats_end)][desc.fingerprint]["hist_write"] = \
                        desc.write_history_values[-1] / desc.write_history_interval
                    for cc in desc.bridge_ips:
                        stats[str(desc.bridge_stats_end)][desc.fingerprint][cc] += desc.bridge_ips[cc] - 4

        for nick in bridge_list:
            br = Bridge.query.filter_by(nickname=nick.nickname).first()
            for desc_date_end in stats.keys():
                if br.hashed_fingerprint in stats[desc_date_end]:
                    for country in stats[desc_date_end][br.hashed_fingerprint]:
                        uc = UserCounts(country=country,
                                        total_users=stats[desc_date_end][br.hashed_fingerprint][country],
                                        fingerprint=br.fingerprint,
                                        date_end=datetime.datetime.strptime(desc_date_end, '%Y-%m-%d %H:%M:%S'),
                                        id=f"{br.fingerprint}{desc_date_end}{country}"
                                        )
                        exists = UserCounts.query.filter_by(id=f"{br.fingerprint}{desc_date_end}{country}").first()
                        if not exists:
                            print("Added a new descriptor to the database.")
                            db.session.add(uc)
                        db.session.commit()

            for desc_date_end in stats_bytes.keys():
                if br.hashed_fingerprint in stats_bytes[desc_date_end]:
                    bc = ByteCounts(bytes_read=stats_bytes[desc_date_end][br.hashed_fingerprint]["hist_read"],
                                    bytes_written=stats_bytes[desc_date_end][br.hashed_fingerprint]["hist_write"],
                                    id=f"{br.fingerprint}{desc_date_end}",
                                    fingerprint=br.fingerprint,
                                    date_end=datetime.datetime.strptime(desc_date_end, '%Y-%m-%d %H:%M:%S')
                                    )
                    exists_bc = ByteCounts.query.filter_by(id=f"{br.fingerprint}{desc_date_end}").first()
                    if not exists_bc:
                        print("Added a new bytes descriptor to the database.")
                        db.session.add(bc)
                    db.session.commit()
