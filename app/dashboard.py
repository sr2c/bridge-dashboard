import json
import logging
from collections import defaultdict
from datetime import datetime, timedelta

from flask import render_template, flash, request, send_file, make_response, Blueprint
from sqlalchemy import func, desc

from app.extensions import db
from app.models import Bridge, Group, Provider, DistributionMethod, UserCounts, LastUpdates
from app.forms import EditGroupForm, DeleteGroupForm
from app.utils import noauth, auth, load_json, format_update_info, avg_client_graph, bytes_client_graph

dashboard = Blueprint('dashboard', __name__,
                      template_folder='templates')


@dashboard.route('/', methods=['GET'])
@noauth
def public(user):
    bridges = Bridge.query.order_by(desc(Bridge.last_restarted)).all()
    return render_template("index.html.j2", bridges=bridges, user=user, counts_dict={})


@dashboard.route('/groups', methods=['POST', 'GET'])
@auth
def groups(user):
    gs = Group.query.all()
    return render_template('groups.html.j2', title='Edit Groups', groups=gs, user=user)


@dashboard.route('/add_group', methods=['POST', 'GET'])
@auth
def add_group(user):
    form = EditGroupForm()
    group = Group()
    provider_list = [e.name for e in Provider]
    distribution_methods = [e.name for e in DistributionMethod]
    form.provider.choices = provider_list
    form.distribution_method.choices = distribution_methods
    if form.validate_on_submit():
        group.name = form.name.data
        group.provider = form.provider.data
        group.distribution_method = form.distribution_method.data
        group.description = form.description.data
        try:
            db.session.add(group)
            db.session.commit()
            flash('Group has been added.')
        except Exception as e:
            flash(f'Could not add group - error was {e.args[0]}')

    return render_template('add_group.html.j2', title='Add Group', form=form, user=user)


@dashboard.route('/del_group', methods=['POST', 'GET'])
@auth
def del_group(user):
    form = DeleteGroupForm()
    form.name.choices = [i.name for i in db.session.query(Group).all()]
    if form.validate_on_submit():
        name = form.name.data
        group = Group.query.filter_by(name=name).first()
        try:
            db.session.delete(group)
            db.session.commit()
            flash('Group has been deleted.')
        except Exception as e:
            flash(f'Could not add group - error was {e.args[0]}')

    return render_template('add_group.html.j2', title='Delete Group', form=form, user=user)


@dashboard.route('/index', methods=['POST', 'GET'])
@auth
def index(user):  # put application's code here
    bridge_list = Bridge.query.order_by(desc(Bridge.last_restarted)).all()

    counts_dict = defaultdict(lambda: defaultdict(lambda: 0))
    for nick in bridge_list:
        br = Bridge.query.filter_by(nickname=nick.nickname).first()
        users_per_country = db.session.query(func.sum(UserCounts.total_users).label('msum'), UserCounts.country). \
            filter(UserCounts.fingerprint == br.fingerprint,
                   func.DATE(UserCounts.date_end) == datetime.now().date() - timedelta(
                       days=1)).group_by(UserCounts.country).all()

        total_users = db.session.query(func.sum(UserCounts.total_users).label('msum')). \
            filter(UserCounts.fingerprint == br.fingerprint,
                   func.DATE(UserCounts.date_end) == datetime.now().date() - timedelta(
                       days=1)).first()
        counts_dict[br.fingerprint]["total"] = total_users.msum
        counts_dict[br.fingerprint]["per_country"] = users_per_country

    if request.method == 'POST':
        for nick in bridge_list:
            if request.form.get(nick.nickname) == "Change status":
                br_to_change = Bridge.query.filter_by(nickname=nick.nickname).first()
                br_to_change.active = not br_to_change.active
                db.session.commit()
        if request.form.get("output_json") == "Output json file":
            output_dict = dict()
            for b in bridge_list:
                output_dict[b.nickname] = b.active
            with open("static/test.txt", "w") as f:
                f.write(json.dumps(output_dict))
            return send_file("static/test.txt", as_attachment=True)

        u = db.session.query(UserCounts.fingerprint, UserCounts.country,
                             func.DATE(UserCounts.date_end).label('mdate'), UserCounts.total_users, ).group_by(
            UserCounts.fingerprint, UserCounts.country, func.DATE(UserCounts.date_end).label('mdate')).all()

        if request.form.get("export_csv") == "Export CSV of daily users per bridge per country":
            with open("static/output.csv", "w") as f:
                f.write("nick,country,date,users\n")
                for row in u:
                    row_as_string = str(row)
                    f.write(row_as_string[1:-1] + '\n')  ## row_as_string[1:-1] because row is a tuple
            return send_file("static/output.csv", as_attachment=True)
    if LastUpdates.query.all():
        flash(format_update_info(LastUpdates.query.all()))
    return render_template('index.html.j2', title='Dynamic Bridge Dashboard', bridges=bridge_list, counts_dict=counts_dict,
                           user=user)


@dashboard.route('/detail/<fingerprint>', methods=['GET'])
@auth
def show_detail(fingerprint, user):
    br = Bridge.query.filter_by(fingerprint=fingerprint).first()
    graphs = dict()
    try:
        graphs["clients"] = avg_client_graph(br)
        graphs["bytes"] = bytes_client_graph(br)
    except Exception:
        logging.exception("Failed generating graphs")
    return render_template('detail.html.j2', graphs=graphs, br=br, title='Bridge Detail', user=user)


@dashboard.route('/profile', methods=['GET'])
@auth
def profile(user):
    return render_template('user.html.j2', user=user)


@dashboard.route('/add_managed_bridges', methods=['GET', 'POST'])
@auth
def add_managed_bridges(user):
    all_groups = [e.name for e in Group.query.filter(Group.provider != Provider.NONE).all()]
    if request.method == 'POST':
        if request.form.get("add_bridges") == "Add bridges":
            text_lines = [x.strip() for x in request.form.get("bridgelines").split('\r\n')]
            for line in text_lines:
                if line:
                    line_components = line.split(" ")
                    if len(line_components) > 1:
                        flash("Invalid name, could not add bridge. Bridge names must not contain spaces.")
                        continue
                    else:
                        group = Group.query.filter_by(name=request.form.get("group")).first()
                        if group:
                            br = Bridge(name=line_components[0],
                                        group=group.name,
                                        active=True
                                        )
                            exists = Bridge.query.filter_by(name=line_components[0],
                                                            group=group.name).first()
                            print(exists)
                            if not exists:
                                db.session.add(br)
                                db.session.commit()
                                flash(f"Added new bridge {br.name} in group {br.group}")
                            else:
                                flash(
                                    f"Bridge {br.name} already exists in group {br.group}, please chose a different name or group.")
                                continue
                        else:
                            flash(
                                f"{group} does not exist.")

    return render_template('add_bridges.html.j2', title='Add Managed Bridges', all_groups=all_groups, user=user)


@dashboard.route('/add_external_bridges', methods=['GET', 'POST'])
@auth
def add_external_bridges(user):
    all_groups = [e.name for e in Group.query.filter(Group.provider == Provider.NONE).all()]
    if request.method == 'POST':
        if request.form.get("add_bridges") == "Add bridges":
            bridge_group = Group.query.filter_by(name=request.form.get("group")).first()
            text_lines = [x.strip() for x in request.form.get("bridgelines").split('\r\n')]
            for line in text_lines:
                if line:
                    line_components = line.split(" ")
                    if not "Bridge" in line_components or not "obfs4" in line_components:
                        flash("Bridge line does not appear to start with Bridge or obfs4, could not add it.")
                        continue
                    if len(line_components) != 7:
                        flash("Bridge line is not long enough, could not add it")
                        continue
                    ip_address = line_components[2].split(":")[0]
                    obfs_port = line_components[2].split(":")[1]
                    nickname = line_components[3]
                    fingerprint = line_components[4]
                    line_components.pop(3)
                    br = Bridge(nickname=nickname,
                                fingerprint=fingerprint,
                                obfs_port=obfs_port,
                                ip_address=ip_address,
                                bridgeline=" ".join(line_components),
                                group=bridge_group.name
                                )
                    exists = Bridge.query.filter_by(fingerprint=fingerprint).first()
                    if not exists:
                        db.session.add(br)
                        db.session.commit()
                        flash(f"Added new bridge {br.nickname}")
                    else:
                        flash(f"Bridge {br.nickname} already exists")
    return render_template('add_bridges.html.j2', all_groups=all_groups, title='Add External Bridges', user=user)


@dashboard.route('/terratest', methods=['GET'])
@auth
def terratest(_):
    resp = make_response(render_template('main.tf.j2', config={}, groups={}))
    resp.headers['Content-type'] = 'text/plain; charset=utf-8'
    return resp
