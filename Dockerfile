FROM python:3.8-slim-bullseye

COPY . /srv/bridges
WORKDIR /srv/bridges

RUN apt-get -y update \
    && apt-get -y install nginx \
    && apt-get -y install python3-dev \
    && apt-get -y install build-essential \
    && apt-get clean

RUN pip install -r requirements.txt && pip install uwsgi && pip install psycopg2-binary

RUN chmod +x ./container/start.sh
CMD ["./container/start.sh"]